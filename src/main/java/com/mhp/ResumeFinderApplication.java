package com.mhp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResumeFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResumeFinderApplication.class, args);
	}
}
